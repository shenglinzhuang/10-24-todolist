import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    myList:[
       { id: 100, name: "吃饭", isDone: true },
       { id: 101, name: "睡觉", isDone: false },
       { id: 102, name: "打豆豆", isDone: true },
    ],
    filter:0
  },
  mutations: {
    setMyList(state,newList) {
      state.myList.unshift(newList)
    },
    removeList(state, index) {
      state.myList.splice(index,1)
    },
    changeIsDone(state, index) {
      state.myList[index].isDone = !state.myList[index].isDone
    },
    chanegFilterIsDone(state, isDone) {
      state.myList.forEach(item => item.isDone = isDone)
    },
    setFliter(state, i) {
      state.filter = i
    },
    clearList(state) {
      state.myList= state.myList.filter(item => item.isDone !== true)
    }
  },
  getters: {
    newList(state){
      if (state.filter === 1) {
        return state.myList.filter(item => item.isDone === false)
      } else if (state.filter === 2) {
        return state.myList.filter(item => item.isDone === true)
      } else {
        return state.myList
      }
    }
  },
})